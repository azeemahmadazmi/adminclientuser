@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 auth-left">
            <div class="row auth-form mb-4">
                <div class="col-12 col-sm-12">
                    <div class="auth-text-top mb-4">
                        <h1>Welcome Back</h1>
                        <small>Please login to your account or <a href="{{url('register')}}">Create Account</a></small>
                    </div>
                    @if (session('failed_message'))
                        <p class="alert alert-danger">
                            {{ session('failed_message') }}
                        </p>
                    @endif
                    @if (session('flash_message'))
                        <p class="alert alert-success">
                            {{ session('flash_message') }}
                        </p>
                    @endif
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <div class="input-icon">
                                <i class="mdi mdi-email"></i>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <div class="input-icon">
                                <i class="mdi mdi-lock"></i>
                                <span class="passtoggle mdi mdi-eye toggle-password"></span>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter Your Password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="d-flex form-check">
                            <input type="checkbox" class="filter" id="remember" checked>
                            <label for="remember">Remember me</label>
                            <a href="{{url('password/reset')}}" class="ml-auto font-s">Forgot Password?</a>
                        </div>
                        <button class="btn btn-primary btn-block btn-c mt-4 mb-4" type="submit">{{ __('Login') }}</button>
                    </form>
                    <p class="divider">Or</p>
                    <div class="btn-social mb-4">
                        <a href="{{ url('/login/facebook') }}"> <button class="btn bg-primary"><i class="mdi mdi-facebook"></i></button></a>&nbsp;&nbsp;&nbsp;
                        <a href="{{ url('/login/google') }}"><button class="btn bg-danger"><i class="mdi mdi-google"></i></button></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 auth-right d-lg-flex d-none bg-gradient" id="particles">
            <div class="logo">
                <img src="{{asset('assets/img/logo.png')}}" width="100" alt="logo">
            </div>
            <div class="heading">
                <h3>Welcome to AdventureX</h3>
            </div>
            <div class="shape"></div>
        </div>
    </div>
</div>
@endsection