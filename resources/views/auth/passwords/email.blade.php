@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 auth-left">
            <div class="row auth-form mb-4">
                <div class="col-12 col-sm-12">
                    <div class="auth-text-top mb-4 text-center">
                        <h1>Reset Password</h1>
                        <small>Enter your email address and we'll send you an email with instructions to reset your password.</small>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ url('/password/email') }}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <div class="input-icon">
                                <i class="mdi mdi-email"></i>
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Enter Your Email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-c mt-4 mb-4"> {{ __('Send Password Reset Link') }}</button>
                        <div class="auth-text-bottom">
                            <p>Already have account?<a href="{{url('login')}}"> Login to Account</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8 auth-right d-lg-flex d-none bg-gradient" id="particles">
            <div class="logo">
                <img src="{{asset('assets/img/logo.png')}}" width="100" alt="logo">
            </div>
            <div class="heading">
                <h3>Welcome to AdventureX</h3>
            </div>
            <div class="shape"></div>
        </div>
    </div>
</div>
@endsection