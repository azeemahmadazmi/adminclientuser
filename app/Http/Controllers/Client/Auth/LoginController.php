<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Client;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected function guard()
    {
        return Auth::guard('client');
    }
    protected $redirectTo = '/client/home';
    public function __construct()
    {
        $this->middleware('client_guest')->except('logout');
    }
    public function showLoginForm(){
        return view('client.auth.login');
    }

    public function login(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password'=>'required',
        ]);

        $remember_me = $request->has('remember') ? true : false;
        $user = Client::where('email',$request->email)->first();
        if(!$user){
            session()->flash('failed_message','Email Id is not registered with us');
            return redirect('client/login');
        }else if (!Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password], $remember_me)){
            session()->flash('failed_message','Incorrect Password');
            return redirect('client/login');
        }else{
            //dd($user);
            $this->guard()->login($user);
            return redirect($this->redirectTo);
        }
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        return redirect('/client/login')->with('flash_message', 'Logout Successfully !');
    }
}
