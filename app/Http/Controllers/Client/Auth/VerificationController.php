<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Client;


class VerificationController extends Controller
{

    use VerifiesEmails;

    protected $redirectTo = '/client/home';

    public function __construct()
    {
        $this->middleware('client_auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
    public function show(){

        return view('client.auth.verify');
    }

    public function resend(){
        $user=Auth::guard('client')->user();
        $id=$user->id;
        $url = URL::signedRoute('client.verification.verify',['id' => $id], now()->addMinutes(30));
        $data=['link' => $url,'to_email' =>$user->email,'from_email'=>'admin@adventureX.com','subject' =>'Client Verify Email','title' =>'Cient Mail Verification','view' =>'client.email.resendverificationmail'];
        $mailstatus=$this->sendemail($data);

        if($mailstatus==1){
            return redirect()->back()->with('resent','A fresh verification link has been sent to your email address');
        }
        else{
            return redirect()->back()->with('resent','Some thing wrong! . Please resend the email.');
        }

    }
    public function verify(Request $request,$id){
            $admin=Client::find($id);
            $admin->email_verified_at=date('Y-m-d H:i:s');
            $admin->save();
            return redirect('/client/home');

    }
}
